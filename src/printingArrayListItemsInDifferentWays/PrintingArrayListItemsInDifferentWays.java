package printingArrayListItemsInDifferentWays;

import java.util.ArrayList;
import java.util.Iterator;

public class PrintingArrayListItemsInDifferentWays {

	public static void main(String[] args) {
		
		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("Ramu");
		arrayList.add("Mukund");
		arrayList.add("Ishi");
		
		Iterator it = arrayList.iterator();
		System.out.println("Using While");
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
		System.out.println("Using for loop");
		for(int i=0; i< arrayList.size(); i++){
			System.out.println(arrayList.get(i));
		}
		
		System.out.println("Using ForEach loop");
		for(String al: arrayList){
			System.out.println(al);
		}
	}

}

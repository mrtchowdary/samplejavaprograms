package fizzBuzzChallenge;

public class FizzBuzz {

	public static void main(String[] args) {

		String temp;
		for(int i=1; i<=100; i++){
			temp="";
			if(i%3 == 0)
				temp = "fizz";
			if(i%5 == 0)
				temp = "buzz";
			if((i%3 == 0) && (i%5 == 0))
				temp = "fizzbuzz";
			if(temp.equals(""))
				System.out.print(i+" ");
			else
				System.out.print(temp+" ");
		}

	}

}

package swapTwoNumbersUsingThirdVariable;

import java.util.Scanner;

public class SwapTwoNumbersUsingThirdVariable {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter two numbers");
		int x = scan.nextInt();
		int y = scan.nextInt();
		scan.close();
		int tmp;
		System.out.println("Numbers before swapping are " + x + " and "+ y);
		tmp =x;
		x=y;
		y=tmp;
		System.out.println("Numbers after swapping are " + x + " and "+ y);
	}

}

package swapTwoNumbersWithoutUsingThirdVariable;

import java.util.Scanner;

public class SwapTwoNumbersWithoutUsingThirdVariable {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter two numbers");
		int x= scan.nextInt();
		int y = scan.nextInt();
		scan.close();
		System.out.println("Numbers before swaping are " + x + " and "+ y);
		x=x+y;
		y=x-y;
		x=x-y;
		System.out.println("Numbers after swapping are " + x + " and "+ y);

	}

}

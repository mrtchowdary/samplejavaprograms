package iterateHashMapUsingWhileForEach;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class IterateHashMapUsingWhileForEach {

	public static void main(String[] args) {
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("Ramu", "B.tech");
		hm.put("Lucky", "MBA");
		hm.put("Ishita","LKG");
		
		Iterator it = hm.entrySet().iterator();
		System.out.println("using while loop");
		while(it.hasNext()){
			Map.Entry me = (Map.Entry) it.next();
			System.out.println("Key is " + me.getKey() +" and value is "+ me.getValue());
		}
		System.out.println("using for loop");
		for(Map.Entry me2 : hm.entrySet()){
			System.out.println("Key is " + me2.getKey() +" and value is "+ me2.getValue());
		}
	}

}

package reverseStringUsingCharArray;

public class ReversesStringUsingCharArray {

	public static void main(String[] args){
		String str = "Sample program";
		char[] ch = str.toCharArray();
		int len = ch.length;
		for(int i = len-1; i>=0; i--){
			System.out.print(ch[i]);
		}
	}
}

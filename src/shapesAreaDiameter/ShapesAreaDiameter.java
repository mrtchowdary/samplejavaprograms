package shapesAreaDiameter;

import java.util.Scanner;

public class ShapesAreaDiameter {

	public static void main(String[] args) {
		double pi = Math.PI;
		System.out.println("Enter shape : ");
		Scanner scan = new Scanner(System.in);
		String shape = scan.nextLine();
		if(shape.equalsIgnoreCase("hexagon")){
			//(6 * s^2)/(4*tan(π/6))
			System.out.println("Enter lenth of hexagon side");
			double len = scan.nextDouble();
			System.out.println("Area of Hexagon is : "+ (6*len*len)/(4*Math.tan(pi/6)));
		}else if(shape.equalsIgnoreCase("polygon")){
			//(n*s^2)/(4*tan(π/n))
			System.out.println("Enter the sides of polygon");
			int sides = scan.nextInt();
			System.out.println("Enter the length of one of polygon sides");
			double len = scan.nextDouble();
			System.out.println("Area of polygon is : "+ (sides*len*len)/(4*Math.tan(pi/sides)));
		}else if(shape.equalsIgnoreCase("circle")){
			System.out.println("Enter radius of circle");
			double radius = scan.nextDouble();
			System.out.println("Area of circle is : " + pi*radius*radius);
			System.out.println("Perimeter/Diameter of circle is : " +2*pi*radius);
		}else  if(shape.equalsIgnoreCase("rectangle")){
			System.out.println("Enter height of rectangle");
			double height = scan.nextDouble();
			System.out.println("Enter width of rectangle");
			double width = scan.nextDouble();
			System.out.println("Area of rectangle is : " + height*width);
			System.out.println("Perimeter/Diameter of rectangle is : " + 2* (height+width));
		}else if(shape.equalsIgnoreCase("square")){
			System.out.println("Enter length of the side");
			double length = scan.nextDouble();
			System.out.println("Area of square is : "+ length*length);
			System.out.println("Perimeter/Diameter of square is : "+ 4*length);
		}else
			System.out.println("No shapes found with given input");
		scan.close();
	}

}

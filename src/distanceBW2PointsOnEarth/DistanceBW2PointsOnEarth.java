package distanceBW2PointsOnEarth;

public class DistanceBW2PointsOnEarth {

	public static void main(String[] args) {
		
		double lat1 = Math.toRadians(1.25);
		double lon1 = Math.toRadians(1.35);
		double lat2 = Math.toRadians(2.35);
		double lon2 = Math.toRadians(2.25);
		//radius * arccos(sin(x1) * sin(x2) + cos(x1) * cos(x2) * cos(y1 - y2))
		double radius = 6371.01;
		double distance = radius*Math.acos(Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon1-lon2));
		System.out.println(distance);
	}

}

package arrayPrograms;

public class ArrayPrograms {

	public static void main(String[] args) {
		int[] arr = {100,50,25,15,30,99};
		ArrayPrograms ap = new ArrayPrograms();
		
		//Sorting Array
		ap.SortArrayAscending(arr);
		
		//Reverse array elements
//		ap.ReverseArrayElements(arr);
		
		//Average of array
//		ap.AverageOfArrayNumbers(arr);
	}

	public void AverageOfArrayNumbers(int[] arr){
		int total = 0;
		for(int i=0; i<arr.length; i++){
			total += arr[i];
		}
		System.out.println("Average of array numbers is : "+total/arr.length);
	}
	
	public void ReverseArrayElements(int[] arr){
		int len = arr.length;
		
		int i = 0;
		int j = len-1;
		int temp;
		while(i<j){
			temp = arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
			i++;
			j--;
		}
		for(int k=0; k<len;k++){
			System.out.print(arr[k]+ " ");
		}
	}
	
	public void SortArrayAscending(int[] arr){
		int temp;
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
				if(arr[i]>arr[j]){
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}

		System.out.println("Array after sorting is : ");
		for(int i = 0 ; i < arr.length; i++)
			System.out.print(arr[i]+" ");
	}
}

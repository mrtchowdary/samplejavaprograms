package countNumberOfWordsUsingHashMap;

import java.util.HashMap;
import java.util.Scanner;

public class CountNumberOfWordsUsingHashMap {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the sentence");
		String Sentence = scan.nextLine();
		scan.close();
		String[] str = Sentence.split(" ");
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		for(int i = 0; i<str.length; i++){
			if(hm.containsKey(str[i])){
				int count = hm.get(str[i]);
				hm.put(str[i], count+1);
			}else {
				hm.put(str[i], 1);
			}
		}
		System.out.println(hm);
	}

}

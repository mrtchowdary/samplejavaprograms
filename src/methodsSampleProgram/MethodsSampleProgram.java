package methodsSampleProgram;

import java.util.Random;
import java.util.Scanner;

public class MethodsSampleProgram {

	public static void main(String[] args) {
		
		MethodsSampleProgram msp = new MethodsSampleProgram();
		
		msp.CountVowelsConstants("BeginnersBook");
		
		//Reverse words in a string
//		msp.ReverseWordsInString("Sample program to reverse words in a string");
		
		//Arranging Strings in Alphabetical order
//		msp.ArrangeStringInAlphabeticalOrder();
		
		
		//Print n by n matrix
//		msp.PrintMatrix(10);
		
		//Print 20 characters per line between given characters
//		msp.PrintChars('(','z',20);
		
		//Number of words in a string
//		System.out.println("Number of words in the given string are : "+msp.NumberOfWords("The quick brown fox jumps over the lazy dog"));
		
		//Count vowels in given string
//		System.out.println("Vowels count in the string are : "+msp.CountVowels("w3resources"));
		
		//Print middle character(s)
//		System.out.println("Middle characters are : "+msp.PrintMiddleCharacter("RAMU"));
		
		//Average of three numbers
//		System.out.println("Average of the three numbers is : "+msp.AverageOF3Numbers(25, 45, 65));
		
		//insert elements into array and reading
//		int num[] = msp.InputNumbers();
//		for(int i=0; i<=num[0];i++)
//			System.out.print(num[i] + " ");
		
		//smallest of three numbers using Math.min
//		msp.SmallestNumberUsingMath(25, 37, 29);
		
		//smallest of three numbers
//		msp.SmallestNumber(25, 37, 29);
		
	}
	
	public void CountVowelsConstants(String str){
		int vcount = 0, ccount = 0;
		str = str.toLowerCase();
		for(int i=0; i<str.length(); i++){
			char ch = str.charAt(i);
			if(ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u')
				vcount++;
			else if(ch>='a' && ch<='z')
				ccount++;
		}
		System.out.println("Vowels count is "+vcount+"\nConstants count is "+ccount);
	}
	
	public void ReverseWordsInString(String str){
		String[] Words = str.split(" ");
		String Word;
		String Reverse  = "";
		for(int i=0; i<Words.length; i++){
			Word = Words[i];
			int wordlen = Word.length();
			String ReverseWord = "";
			for(int j=wordlen-1; j>=0; j--){
				ReverseWord = ReverseWord+Word.charAt(j);
			}
			ReverseWord = ReverseWord + " ";
			Reverse = Reverse + ReverseWord;
		}
		System.out.println("Input String is : " + str);
		System.out.println("Reverse string is : " + Reverse);
	}
	
	public void ArrangeStringInAlphabeticalOrder(){
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the number of strings to be sorted");
		int count = scan.nextInt();

		Scanner scan1= new Scanner(System.in);
		String string[] = new String[count];
		System.out.println("Enter "+ count + " Strings");
		for(int i=0; i<count; i++){
			string[i] = scan1.nextLine();
		}
		scan.close();
		scan1.close();
		
		String temp;
		for(int i = 0; i<count; i++){
			for(int j=i+1; j<count; j++){
				if(string[i].compareTo(string[j]) > 0){
					temp = string[i];
					string[i] = string[j];
					string[j] = temp;
				}
			}
		}
		
		System.out.println("Strings after sorting are");
		for(int k=0; k<count; k++){
			System.out.println(string[k]);
		}
	}
	
	public void PrintMatrix(int n){
		for(int i=0; i<n; i++){
			for(int j=0; j<n; j++){
				System.out.print((int)(Math.random()*2)+ " ");
			}
			System.out.println();
		}
	}
	
	public void PrintChars(char c1, char c2, int count){
		for(int len = 1; c1<=c2; c1++,len++){
			System.out.print(c1+" ");
			if(len%20 == 0)	System.out.println();
		}
		
	}
	
	public int NumberOfWords(String str){
		int count = 0;
		String[] arr = str.split(" ");
		if(str.length()>0)
			count = arr.length;
		return count;
	}
	
	public int CountVowels(String str){
		int count = 0;
		for(int i=0; i<str.length(); i++){
			if((str.charAt(i)=='a')||(str.charAt(i)=='e')||(str.charAt(i)=='i')
					||(str.charAt(i)=='o')||(str.charAt(i)=='u'))
				count++;
		}
		return count;
	}
	
	public String PrintMiddleCharacter(String str){
		int len=0,pos=0;
		
		if(str.length()%2 == 0){
			pos=str.length()/2-1;
			len=2;
		}else{
			pos=str.length()/2;
			len=1;
		}
		return str.substring(pos, pos+len);
	}

	public int AverageOF3Numbers(int num1, int num2, int num3){
		return (num1+num2+num3)/3;
	}
	
	public void SmallestNumber(int num1, int num2, int num3){
		if(num1<num2){
			if(num1<num3)
				System.out.println("Smallest number is : " + num1);
			else
				System.out.println("Smallest number is : " + num3);
		}else if(num3<num2){
			System.out.println("Smallest number is : " + num3);
		}else{
			System.out.println("Smallest number is : " + num2);
		}
	}
	
	public void SmallestNumberUsingMath(int num1, int num2, int num3){
		System.out.println("Smallest number is : " +Math.min(Math.min(num1, num2),num3));
	}
	
	public int[] InputNumbers(){
		int num[]={0,0,0,0,0,0,0,0,0,0};
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the count of number");
		int count = scan.nextInt();
		num[0] = count;
		if(count>10)
			System.out.println("Count can't be greater then 10");
		else{
			for(int i=1; i<=count; i++){
				System.out.print("Enter "+i+" number");
				num[i]=scan.nextInt();
			}
		}
		scan.close();
		return num;
	}
}

package threadPractice;

public class ThreadPractice {

	public static void main(String[] args) {

		Thread t = new Thread();
		t.start();
		System.out.println("ID : "+t.getId());
		System.out.println("Name : "+t.getName());
		System.out.println("Priority : "+t.getPriority());
		System.out.println("State : "+t.getState());
	}

}

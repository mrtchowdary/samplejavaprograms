package reverseStringUsingReverseMethod;

public class ReverseStringUsingReverseMethod {

	public static void main(String[] args) {
		
		String str = "Sample program";
		
		StringBuilder sb = new StringBuilder();
		sb.append(str);
		sb.reverse();
		System.out.println(sb);
	}

}

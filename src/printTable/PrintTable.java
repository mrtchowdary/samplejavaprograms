package printTable;

import java.util.Scanner;

public class PrintTable {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter table number to be printed");
		int num = scan.nextInt();
		scan.close();
		for(int i=1; i<=10; i++){
			System.out.println(num + "x" + i + " = " + num*i);
		}
	}

}

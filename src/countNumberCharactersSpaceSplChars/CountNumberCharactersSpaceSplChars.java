package countNumberCharactersSpaceSplChars;

import java.util.Scanner;

public class CountNumberCharactersSpaceSplChars {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = scan.nextLine();
		scan.close();
		
		char[] ch = str.toCharArray();
		int characters = 0;
		int numbers = 0;
		int spaces = 0;
		int splchar = 0;
		
		for(int i=0; i<str.length(); i++){
			if(Character.isDigit(ch[i]))
				numbers++;
			else if(Character.isSpaceChar(ch[i]))
				spaces++;
			else if(Character.isLetter(ch[i]))
				characters++;
			else
				splchar++;
		}
		System.out.println("Character : "+characters);
		System.out.println("Number : "+numbers);
		System.out.println("Spaces : "+spaces);
		System.out.println("Special characters : "+splchar);
	}
	

}

package reverseStringUsingSplit;

import java.util.Scanner;

public class ReverseStringUsingSplit {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter string to reverse");
		String str = scan.nextLine();
		scan.close();
		String[] arr = str.split("");
		for(int i = arr.length-1; i>=0; i--){
			System.out.print(arr[i]);
		}

	}

}

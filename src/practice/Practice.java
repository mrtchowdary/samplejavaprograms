package practice;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class Practice {

	public static void main(String[] args) {
		
		System.out.println(System.getProperty("os.arch"));
		
		//Find the largest element int the array using collections
//		Integer[] arr = {5,9,7,2,6,1};
//		System.out.println("Largest element is : " +Collections.max(Arrays.asList(arr)));
		
		//Max/Min values of primitive types
//		  System.out.println("Short MAX value is " + Short.MAX_VALUE);
//        System.out.println("Short MIN value is " + Short.MIN_VALUE);
//        System.out.println("Integer MAX value is " + Integer.MAX_VALUE);
//        System.out.println("Integer MIN value is " + Integer.MIN_VALUE);
//        System.out.println("Long MAX value is " + Long.MAX_VALUE);
//        System.out.println("Long MIN value is " + Long.MIN_VALUE);
		
		//removing all vowels from the string
//		String str = "Alive is awesome";
//		System.out.println(str.replaceAll("[aeiouAEIOU]", ""));
		
		//find ASCII value of character
//		int i = 'z';
//		System.out.println(i);
		
		//Square root of number
//		System.out.println("Square root is : " + Math.sqrt(16));
		
		//printing count of each character in the given string
//		String str = "ramatarakachowdarymaddipudi";
//		HashMap<Character, Integer> hm = new HashMap<Character, Integer>();
//		char ch = ' ';
//		int i=0;
//		while(i<str.length()){
//			ch=str.charAt(i);
//			if(hm.containsKey(ch))
//				hm.put(ch, hm.get(ch)+1);
//			else
//				hm.put(ch, 1);
//			i++;
//		}
//		System.out.println(hm);
		
		//Print factorial of a number
//		int n = 4, f=1;
//		for(int i=n; i>0; i--){
//			f*=i;
//		}
//		System.out.println(f);
		
		//Find first non repeated character
//		String str = "hello";
//		Map<Character,Integer> map = new HashMap<Character, Integer>();
//		int i = 0;
//		char ch = ' ';
//		while(i<str.length()){
//			ch=str.charAt(i);
//			if(map.containsKey(ch))
//				map.put(ch, map.get(ch)+1);
//			else
//				map.put(ch, 1);
//			i++;
//		}
//		for(i=0; i<str.length(); i++){
//			ch=str.charAt(i);
//			if(map.get(ch)==1){
//				System.out.println("First non repeated character is : "+ ch);
//				break;
//			}
//		}
		
		//Print multiples of 4 by multiplying with 10
//		int num = 4;
//		int[] arr = new int[num];
//		for(int i=1; i<=num; i++){
//			if(i%4==0)
//				arr[i-1] = i*10;
//			else
//				arr[i-1] = i;
//			System.out.println(arr[i-1]);
//		}
		
		//collecting characters at odd and even places and printing them together
//		String str = "abcd";
//		String streven = "", strodd = "";
//		int i = 0, len = str.length();
//		while (i<len){
//			if(i%2 == 0)
//				streven += str.charAt(i);
//			else
//				strodd += str.charAt(i);
//			i++;
//		}
//		System.out.println(streven+strodd);
		
		//print true if first < word < last else false
//		String first = "bookend";
//		String last = "boolean";
//		String word = "boost";
//		
//		if(first.compareTo(word)<0 && last.compareTo(word)>0) 
//			System.out.println("True");
//		else
//			System.out.println("False");
		
		//if input array is {1,2,3} output should be {1,3,6} (sum of previous items) 
//		int[] nums = {-5,2,3,4};
//		int[] num = new int[nums.length];
//		for(int i=0; i<nums.length; i++){
//			if(i>0){
//				for(int j=0; j<=i; j++){
//					num[i] = num[i]+nums[j];
//				}
//			}else
//				num[i] = nums[i];
//			System.out.println(num[i]);
//		}
		
		//replaceing first character as capital
//		String str = "ramr";
//		System.out.println(str.substring(0, 1).toUpperCase()+str.substring(1));
		
		//return true if  word has two consecutive identical letters.
//		String str = "Raamu";
//		System.out.println(str.matches(".*(.)\\1.*"));
		//Find the number of digits between minimum and maximum numbers
//		int[] ar = {44, 32, 86, 19};
//		Arrays.sort(ar);
//		int d = Math.abs(ar[ar.length-1] - ar[0]);
//		System.out.println(d);
		
		//Check if average of array is whole number
//		int[] ar = {10, 10};
//		float sum=0f, avg=0f;
//		for(int i:ar)
//			sum+=i;
//		avg=sum/ar.length;
//		if(avg%1 == 0)
//			System.out.println("Average of array is whole number : "+avg);
//		else
//			System.out.println("Average of array is not a whole number : "+avg);
		
		
		//remove all spaces in the string
//		String str = " Hello   this is  sample program    ";
//		System.out.println("Before : "+str);
//		str=str.replaceAll(" ", "");
//		System.out.println("After : "+str);
		
		//removing non ASCII characters
//		String str = "Instance��of��java";
//		System.out.println(str);
//		str = str.replaceAll("[^\\p{ASCII}]", "");
//		System.out.println("After removing non ASCII chars:");
//		System.out.println(str);
		
		//print penultimate word
//		String str ="The quick brown fox jumps over the lazy dog.";
//		String[] str1 = str.split(" ");
//		System.out.println(str1[str1.length-2]);
		
		//numbers between x and y that are devisible by p
//		int x=5, y=20, p=3, count =0;
//		for(int i=x+1; i<y; i++){
//			if(i%p == 0)
//				count++;
//		}
//		System.out.println("Count is : " + count);
		
		//print seconds to hh mm ss
//		int num = 86399;
//		int hh = num/60/60;
//		int mm = (num/60)%60;
//		int sec = num%60;
//		System.out.println(hh+":"+mm+":"+sec);
		
		//print string to integer
//		String str = "25";
//		System.out.println(Integer.parseInt(str));
		
		//print numbers between 1& 100 divisible by 3,5 and both 3&5
//		System.out.println("Divisible by 3");
//		for(int i = 3; i<100;i++){
//			if(i%3 == 0){
//				System.out.print(i + " ");
//			}
//		}
//		
//		System.out.println("\n\nDivisible by 5");
//		for(int i = 5; i<100;i++){
//			if(i%5 == 0){
//				System.out.print(i + " ");
//			}
//		}
//		
//		System.out.println("\n\nDivisible by 3 & 5");
//		for(int i = 3; i<100;i++){
//			if(i%3 == 0  && i%5 == 0){
//				System.out.print(i + " ");
//			}
//		}
		
		//print odd numbers from 1 to 99
//		for(int i=1; i<=99; i++){
//			if(i%2 != 0)
//				System.out.println(i);
//		}
		
		//printing ascii value
//		int ch = 'a';
//		System.out.println(ch);
		
		//input n output should be n+nn+nnn
//		int n= 5;
//		System.out.printf("%d + %d%d + %d%d%d\n",n,n,n,n,n,n);
		
		//Print system date time
//		System.out.format("\nCurrent Date time: %tc%n\n",System.currentTimeMillis());

		//Print system date time in specified format
//		SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss:SS");
//		sdt.setCalendar(Calendar.getInstance(TimeZone.getTimeZone("GMT")));
//		System.out.println("\nCurrent Date Time "+sdt.format(System.currentTimeMillis()));
	}

}

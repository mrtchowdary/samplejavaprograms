package collectionListSamplePrograms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CollectionListSamplePrograms {

	public static void main(String[] args) {

		ArrayList<String> ar = new ArrayList<String>();
		ar.add("Blue");	
		ar.add("Red");
		ar.add("Black");
		ar.add("Grey");
		ar.add("Yellow");
		System.out.println("Basic array " + ar);
		
		ar.add("Orange");
		System.out.println(ar);
		ar.add(0, "Pink");
		System.out.println(ar);
		Collections.sort(ar);
		System.out.println(ar);
		ar.remove("Pink");
		System.out.println(ar);
		Collections.reverse(ar);
		System.out.println(ar);
		
		ArrayList<String> ar1 = new ArrayList<String>();
		ar1.add("purple");
		ar1.add("voilet");
		System.out.println(ar1);
		ar.addAll(ar1);
		System.out.println(ar);
		Collections.shuffle(ar);
		System.out.println(ar);
		ar.set(1, "White");
		System.out.println(ar);
	}

}

package getFirstNonRepeatedChar;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class getFirstNonRepeatedChar {

	public static void main(String[] args) {
		
		String str = "array";
		char carr[] = str.toCharArray();
		for(int i=0; i <str.length(); i++){
			if(str.lastIndexOf(carr[i]) == str.indexOf(carr[i]))
				System.out.println("First non repeated character is : "+ carr[i]);
		}
		
//		Map<Character, Integer> map = new LinkedHashMap<Character, Integer>();
//		for(int i= 0; i<str.length(); i++){
//			char c = str.charAt(i);
//			if(map.containsKey(c)){
//				int count = map.get(c);
//				map.put(c, count+1);
//			}else{
//				map.put(c, 1);
//			}
//		}
//		System.out.println(map);
//		for(Entry<Character, Integer> e : map.entrySet()){
//			if(e.getValue()==1){
//				System.out.println("First non repeated character is : "+e.getKey());
//			}
//		}
	}

}

package patternPrinting;

public class PatternPrinting {

	public static void main(String[] args) {
		/*	1
			1 2
			1 2 3
			1 2 3 4
			1 2 3 4 5
			1 2 3 4 5 6
			1 2 3 4 5 6 7	*/
		
//		for(int i=1; i<=7; i++){
//			for(int j=1; j<=i; j++){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
		
		/*	1
			1 2
			1 2 3
			1 2 3 4
			1 2 3 4 5
			1 2 3 4 5 6 7
			1 2 3 4 5 6
			1 2 3 4 5
			1 2 3 4
			1 2 3
			1 2
			1				*/
		
//		for(int i=1; i<=7; i++){
//			for(int j=1; j<=i; j++){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
//		for(int i=6; i>=1; i--){
//			for(int j=1; j<=i; j++){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}

	
		/*	1
			2 2
			3 3 3
			4 4 4 4
			5 5 5 5 5
			6 6 6 6 6 6
			7 7 7 7 7 7 7 */
		
//		for(int i=1; i<=7; i++){
//			for(int j=1; j<=i; j++){
//				System.out.print(i+" ");
//			}
//			System.out.println();
//		}
		
		/*	7 6 5 4 3 2 1
			7 6 5 4 3 2 
			7 6 5 4 3 
			7 6 5 4
			7 6 5
			7 6
			7				*/
		
//		for(int i=1; i<=7; i++){
//			for(int j=7; j>=i; j--){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
		
		/*	1 2 3 4 5 6 7
			1 2 3 4 5 6
			1 2 3 4 5
			1 2 3 4
			1 2 3
			1 2
			1				*/
		
//		for(int i=7; i>0; i--){
//			for(int j=1; j<=i; j++){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
		
		/*	7 6 5 4 3 2 1
			6 5 4 3 2 1
			5 4 3 2 1
			4 3 2 1
			3 2 1
			2 1
			1				*/
		
//		for(int i=7; i>0; i--){
//			for(int j=i; j>0; j--){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
		
		/*	7
			7 6 
			7 6 5
			7 6 5 4
			7 6 5 4 3
			7 6 5 4 3 2 
			7 6 5 4 3 2 1	*/
		
//		for(int i=7; i>0; i--){
//			for(int j=7; j>=i; j--){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
		
		/*	1
			1 2 1
			1 2 3 2 1
			1 2 3 4 3 2 1
			1 2 3 4 5 4 3 2 1
			1 2 3 4 5 6 5 4 3 2 1
			1 2 3 4 5 6 7 6 5 4 3 2 1	*/
		
//		for(int i=1; i<=7; i++){
//			for(int j=1; j<=i; j++){
//				System.out.print(j+" ");
//			}
//			for(int k=i-1; k>0; k--){
//				System.out.print(k+" ");
//			}
//			System.out.println();
//		}
		
		/*	1 2 3 4 5 6 7
			1 2 3 4 5 6
			1 2 3 4 5
			1 2 3 4
			1 2 3
			1 2 
			1 
			1 2
			1 2 3 
			1 2 3 4 
			1 2 3 4 5
			1 2 3 4 5 6
			1 2 3 4 5 6 7	*/
		
//		for(int i=7; i>=1; i--){
//			for(int j=1; j<=i; j++){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
//		for(int i=2; i<=7; i++){
//			for(int j=1; j<=i; j++){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
		
		/*	1234567
			 234567
			  34567
			   4567
			    567
			     67
			      7
			     67
			    567
			   4567
			  34567
			 234567
			1234567			*/

//		for(int i=1; i<=7; i++){
//			for(int j=1; j<i; j++){
//				System.out.print(" ");
//			}
//			for(int k=i; k<=7; k++){
//				System.out.print(k);
//			}
//			System.out.println();
//		}
//		for(int i=6; i>=1; i--){
//			for(int j=1; j<i; j++){
//				System.out.print(" ");
//			}
//			for(int k=i; k<=7; k++){
//				System.out.print(k);
//			}
//			System.out.println();
//		}
		
		/*	1
			2 1
			3 2 1
			4 3 2 1
			5 4 3 2 1 
			6 5 4 3 2 1
			7 6 5 4 3 2 1	*/
		
//		for(int i=1; i<=7; i++){
//			for(int j=i; j>=1; j--){
//				System.out.print(j+" ");
//			}
//			System.out.println();
//		}
		
		/*	1
			10
			101
			1010
			10101
			101010 
			1010101		*/
		
//		for(int i=1; i<=7; i++){
//			for(int j=1; j<=i; j++){
//				if(j%2==0)
//					System.out.print(0);
//				else
//					System.out.print(1);
//			}
//			System.out.println();
//		}
		
		/*	1  2  3  4  5  6  7 
			  2  3  4  5  6  7
			    3  4  5  6  7
			      4  5  6  7
			        5  6  7
			          6  7
			            7
			          6  7
			        5  6  7
			      4  5  6  7
			    3  4  5  6  7
			  2  3  4  5  6  7
			1  2  3  4  5  6  7		*/
		
//		for(int i=1; i<=7; i++){
//			for(int j=1; j<=i; j++){
//				System.out.print(" ");
//			}
//			for(int j=i; j<=7; j++){
//				System.out.print(j+ " ");
//			}
//			System.out.println();
//		}
//		for(int i=6; i>=1; i--){
//			for(int j=1; j<=i; j++){
//				System.out.print(" ");
//			}
//			for(int j=i; j<=7; j++){
//				System.out.print(j+ " ");
//			}
//			System.out.println();
//		}
		
		/*	1111111
			1111122
			1111333
			1114444
			1155555
			1666666
			7777777 	*/
		
		for(int i=1; i<=7; i++){
			for(int j=1; j<=7-i; j++){
				System.out.print(1);
			}
			for(int j=1; j<=i; j++){
				System.out.print(i);
			}
			System.out.println();
		}
	}
}

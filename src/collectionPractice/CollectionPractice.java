package collectionPractice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CollectionPractice {

	public static void main(String[] args) {
		
		CollectionPractice cp = new CollectionPractice();
		//cp.listCollection();
		//cp.hashSetCollection();
		cp.mapCollection();
	}
	
	public void listCollection(){
		List<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(9);
		list.add(8);
		list.add(3);
		list.add(4);
		
		System.out.println("list after adding items : "+ list);
		
		if(list.contains(19))
			System.out.println("List is having it");
		else
			System.out.println("List is not having it");
		
		//converting integer list to array
		int[] arr = new int[list.size()];
		Integer[] iarr = list.toArray(new Integer[list.size()]);
		int j=0;
		System.out.print("array output is : " );
		for(int i:iarr){
			arr[j] = i;
			System.out.print(arr[j]+" ");
			j++;
		}
		
		//converting string list to array
		String[] arr2 = new String[list.size()];
		List<String> list2 = new LinkedList<String>();
		list2.add("hi");
		list2.add("hello");
		list2.add("hey");
		list2.toArray(arr2);
		
		list.add(3, 6);
		System.out.println("List after inserting element at a index : "+list);
		
		list.add(7);
		System.out.println("List after inserting element : "+list);
		
		Collections.sort(list);
		System.out.println("\nList after sorting : "+list);
		
		Collections.reverse(list);
		System.out.println("List after reverse sorting : "+list);
		
		list.remove(4);
		System.out.println("List after removing a element at index : "+list);
	}

	public void hashSetCollection(){
		Set<String> set = new HashSet<String>();
		set.add("black");
		set.add("green");
		set.add("orange");
		set.add("blue");
		set.add("grey");
		
		System.out.println("Set after adding is : "+set);
		
		Iterator<String> itr = set.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		
		if(set.contains("pink"))
			System.out.println("Set is having it");
		else
			System.out.println("Set is not having it");
		
		String arr[] = new String[set.size()];
		set.toArray(arr);
	}
	
	public void mapCollection(){
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(33, "ramu");
		map.put(29, "lucky");
		map.put(4, "ishi");
		map.put(2, "mukund");
		
		System.out.println("Map is "+ map);
		
		System.out.println("Key set is : "+map.keySet());
		
		System.out.println("Values in map are : "+map.values());
		
		Map<Integer, String> mp = new TreeMap<Integer, String>(map);
		
		System.out.println("Sorted on using tree map : "+mp);
		
		Iterator<Integer> it = mp.keySet().iterator();
		while(it.hasNext()){
			int key = it.next();
			System.out.println("Name : " +mp.get(key)+ " Age : "+key);
		}
		
		mp.remove(33);
		System.out.println("Map after removing : "+mp);
	}
}
